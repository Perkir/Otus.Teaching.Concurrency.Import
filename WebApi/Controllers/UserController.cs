﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly ICustomerRepository _repository;
        private readonly ILogger<UserController> _logger;

        public UserController(ICustomerRepository repository, ILogger<UserController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogInformation("Request one customer");
            try
            {
                _logger.LogInformation("Request customer by id: " + id);
                var t = await _repository.GetCustomerByIdAsync(id);
                if (t != null) return Ok(t);
                _logger.LogInformation("Not found customer by id: " + id);
                return NotFound(Response);
            }
            catch (Exception)
            {
                _logger.LogInformation("Fail request customer by id: " + id);
                throw;
            }   
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Customer value)
        {
            try
            {
                _logger.LogInformation("Post request customer");
                _repository.CheckUser(value);
                var status = await _repository.AddCustomerAsync(value);
                _logger.LogInformation("Post request customer success");
            }
            catch (CustomerException)
            {
                _logger.LogInformation("Post request customer fail");
                return Conflict(Response);
            }
            catch (Exception)
            {
                _logger.LogInformation("Post request customer fail");
                throw;
            }
            return Ok(value);
        }
    }
}