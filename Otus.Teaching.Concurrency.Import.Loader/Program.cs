﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.XmlGenerator;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath;
        private static IDataParser<List<Customer>> _dataParser;
        private static IDataLoader _dataLoader;
        
        static async Task Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(ProcessExit);
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            else
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("settings.json")
                    .Build();
                var config = builder.GetSection("DataGeneratorSettings").Get<DataGeneratorSettings>();
                GenerateCustomersDataFile(config);
            }
            
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var stopwatch = new Stopwatch();
            
            _dataParser = new CsvParser(_dataFilePath);
            var data = _dataParser.Parse();
            
            Console.WriteLine($"Начало загрузки {DateTime.Now}");
            stopwatch.Start();
           
            _dataLoader = new ThreadPoolDataLoader(8);
            _dataLoader.LoadData(data);
            Console.WriteLine($"Затрачено времени {stopwatch.Elapsed}");
           
            stopwatch.Start();
           
            _dataLoader = new ThreadDataLoader(8);
            _dataLoader.LoadData(data);
            Console.WriteLine($"Затрачено времени {stopwatch.Elapsed}");
                
            stopwatch.Start();
           
            _dataLoader = new CsvBaseDataLoader();
            _dataLoader.LoadData(data);
            Console.WriteLine($"Затрачено времени {stopwatch.Elapsed}");
            
            stopwatch.Start();
           
            _dataLoader = new AsyncDataLoader();
            await _dataLoader.LoadDataAsync(data);
            Console.WriteLine($"Затрачено времени {stopwatch.Elapsed}");
        }

        private static void ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine($"Конец процесса {DateTime.Now}");
        }

        static void GenerateCustomersDataFile(DataGeneratorSettings config)
        {
            if (config.UseProcess && File.Exists(config.ProcessGeneratorPath))
            {
                Process.Start(new ProcessStartInfo(config.ProcessGeneratorPath,
                    $"{config.GeneratedDataFileName} {config.Records}"));
                return;
            } 
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{config.GeneratedDataFileName}.csv");
            var generator = GeneratorFactory.GetCsvGenerator($"{config.GeneratedDataFileName}.csv", config.Records);
            generator.GenerateAsync();
            _dataFilePath = path;
        }
    }
}