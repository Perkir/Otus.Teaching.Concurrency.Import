namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class DataGeneratorSettings
    {
        public bool UseProcess { get; set; }
        public string ProcessGeneratorPath { get; set; }
        public int Records{ get; set; }
        public string GeneratedDataFileName { get; set; }
    }
}