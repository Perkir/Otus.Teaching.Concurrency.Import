using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class AsyncDataLoader: IDataLoader
    {
        private const int Attempts = 3;
        public void LoadData(List<Customer> data)
        {
            throw new System.NotImplementedException();
        }
        
        public async Task LoadDataAsync(IEnumerable<Customer> data)
        {
            var task = SaveToDatabaseAsync(data.ToArray(), Attempts);
            await Task.WhenAll(task);
        }
        private async Task SaveToDatabaseAsync(IEnumerable<Customer> customers, int attempts)
        {
            try
            {
                await SaveRecordAsync(customers);
                return;
            }
            catch(Exception ex) when (attempts-- >= 0) 
            {
                Console.WriteLine($"{ex.Message}");
                if (attempts == 0)
                {
                    return;
                }
            }
            await SaveToDatabaseAsync(customers, attempts);
        }
        
        private async Task SaveRecordAsync(IEnumerable<Customer> customers)
        {
            using var context = new DataContext();
            var repository = new CustomerRepository(context);
            foreach (var customer in customers)
                await repository.AddCustomerAsync(customer);
        }
        
    }
}