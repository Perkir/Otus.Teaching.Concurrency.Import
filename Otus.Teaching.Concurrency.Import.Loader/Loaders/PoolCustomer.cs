using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class PoolCustomer
    {
        public IEnumerable<Customer> Customers { get; set; }
    }
}