using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class CsvBaseDataLoader : IBaseDataLoader
    {
        private string FileName { get; set; }
        public ICustomerRepository Repository { get; set; }
        public CsvBaseDataLoader() {
            using var context = new DataContext();
            Repository = new CustomerRepository(context);
        }

        public void LoadData(List<Customer> data)
        {
            var csvParser = new CsvParser(FileName);
            List<Customer> customers = csvParser.Parse();
            Repository.AddCustomer(customers);
        }

        public Task LoadDataAsync(IEnumerable<Customer> data)
        {
            throw new NotImplementedException();
        }

        private void SetFileName(string filename)
        {
            FileName = filename;
        }

        public IBaseDataLoader Clone()
        {
            var result = (CsvBaseDataLoader)MemberwiseClone();
            result.SetFileName(FileName);
            return result;
        }
    }
}
