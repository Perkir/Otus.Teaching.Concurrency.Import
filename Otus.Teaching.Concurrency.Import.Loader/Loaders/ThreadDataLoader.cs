using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadDataLoader : IDataLoader
    {
        private readonly int _countThreads;
        public ThreadDataLoader(int countThreads)
        {
            _countThreads = countThreads;
        }

        public void LoadData(List<Customer> customersData)
        {
            using var context = new DataContext();
            var countThreads = _countThreads;
            var customers = customersData;
            for (var i = 0; i < countThreads; i++)
            {
                var items = customers.Count() / countThreads;
                var j = i;
                var thread = new Thread(() => Load( context, customers.Skip(j * items).Take(items).ToList()));
                thread.Name = $"{thread}{j.ToString()}";
                thread.Start();
            }
        }

        public Task LoadDataAsync(IEnumerable<Customer> data)
        {
            throw new System.NotImplementedException();
        }

        private static void Load(DataContext  context, IReadOnlyCollection<Customer> data)
        {
            const int batchSize = 30000;
            var iterationSize = data.Count() / batchSize;
            for (var x = 0; x < iterationSize; x++)
            {
                SaveBatch(context, data.Skip(x * batchSize).Take(batchSize).ToList());
            }
        }

        private static void SaveBatch(DataContext context, IEnumerable<Customer> customers)
        {
            var repository = new CustomerRepository(context);
            repository.AddCustomers(customers.ToList());
            repository.Save();
        }
    }
}