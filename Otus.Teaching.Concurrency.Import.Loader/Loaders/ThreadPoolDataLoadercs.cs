using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadPoolDataLoader: IDataLoader
    {
        private int _countThreads;
        private readonly AutoResetEvent _eventWaitThread = new AutoResetEvent(false);
        public ThreadPoolDataLoader(int countThreads)
        {
            _countThreads = countThreads;
        }
        public void LoadData(List<Customer> data)
        {
            LoadOnThreadPoolThread(new PoolCustomer { Customers = data.ToArray()});
            _eventWaitThread.WaitOne(10 * 1000 * 60);
        }

        public Task LoadDataAsync(IEnumerable<Customer> data)
        {
            throw new NotImplementedException();
        }

        private void LoadOnThreadPoolThread(PoolCustomer data) =>
            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveToDatabase), data);

        private void SaveToDatabase(object customers)
        {
            customers = customers as PoolCustomer;
            using var context = new DataContext();
            var repository = new CustomerRepository(context);
            try
            { 
                foreach (var customer in customers as IEnumerable<List<Customer>>)
                    repository.AddCustomer(customer);
                repository.Save();
                Console.WriteLine($"{customers}saved.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{customers}, {ex}");
            }
        }
    }

}