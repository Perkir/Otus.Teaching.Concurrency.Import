using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader : IDataLoader
    {
        public void LoadData(List<Customer> data)
        {
            Console.WriteLine("Loading data...");
            Thread.Sleep(10000);
            Console.WriteLine("Loaded data...");
        }

        public Task LoadDataAsync(IEnumerable<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}