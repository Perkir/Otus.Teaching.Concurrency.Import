using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository: IDisposable
    {
        Task<List<Customer>> AddCustomer(IEnumerable<Customer> customers);
        Task Save();
        Task<Customer> GetCustomerByIdAsync(int id);
        Task<int> AddCustomerAsync(Customer user);
        int CheckUser(Customer user);
    }
}