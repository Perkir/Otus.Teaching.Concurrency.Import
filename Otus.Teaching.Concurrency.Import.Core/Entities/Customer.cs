using Newtonsoft.Json;

namespace Otus.Teaching.Concurrency.Import.Core.Entities
{
    public class Customer
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{Id},{FullName},{Email},{Phone}";
        }

    }
}