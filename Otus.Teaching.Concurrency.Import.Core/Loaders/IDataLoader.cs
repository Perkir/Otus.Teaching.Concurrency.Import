﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        void LoadData(List<Customer> data);
        Task LoadDataAsync(IEnumerable<Customer> data);
    }
}