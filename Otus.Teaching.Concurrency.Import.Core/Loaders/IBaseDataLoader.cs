using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IBaseDataLoader : IDataLoader
        {
            IBaseDataLoader Clone();
            ICustomerRepository Repository { get; set; }
        }
}