using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class JsonGenerator: IDataGenerator
    {
        private readonly int _customerId;
        private readonly string _fileName;
        public string CustomerJson { get; set; }
        
        public JsonGenerator(int customerId, string fileName)
        {
            _customerId = customerId;
            _fileName = fileName;
        }
        public void Generate()
        {
            var customer = RandomCustomerGenerator.GenerateById(_customerId);
            CustomerJson = JsonSerializer.Serialize(customer);
        }

        public async Task<string> GenerateAsync()
        {
            var customer =  RandomCustomerGenerator.GenerateById(_customerId);
            await using var fs = new FileStream(_fileName, FileMode.OpenOrCreate);
              await JsonSerializer.SerializeAsync(fs, customer);
            return _fileName;
        }
    }
}