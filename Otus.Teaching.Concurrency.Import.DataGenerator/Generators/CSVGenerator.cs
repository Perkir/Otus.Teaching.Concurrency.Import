using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CSVGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CSVGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.CreateText(_fileName);
            foreach (var item in customers)
            {
                stream.WriteLine(item.ToString());
            }
        }
        public async Task<string> GenerateAsync()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            await using var stream = File.CreateText(_fileName);
            foreach (var item in customers)
            {
                await stream.WriteAsync(item.ToString());
            }
            return _fileName;
        }
    }
}
