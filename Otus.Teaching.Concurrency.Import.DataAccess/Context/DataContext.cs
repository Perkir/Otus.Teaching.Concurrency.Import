using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Context
{
    public class DataContext: DbContext
    {
        public DataContext()
        {
           // Database.EnsureDeleted();
           // Database.EnsureCreated();
        }
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=Customers.db");
        public DbSet<Customer> Customers { get; set; }
    }
}