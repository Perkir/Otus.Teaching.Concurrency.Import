using System.Collections.Generic;
using System.IO;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using ServiceStack;
using ServiceStack.Text;


namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _path;
        private CsvSerializer _serializer;
        public CsvParser(string path)
        {
            _path = path;
            _serializer = new CsvSerializer();
        }


        public List<Customer> Parse()
        {
            var list = new CustomersList();
            list.Customers = new List<Customer>();
            list.Customers.AddRange(File.ReadAllText(_path).FromCsv<List<Customer>>());
            return list?.Customers ?? null;
        }

    }
}
