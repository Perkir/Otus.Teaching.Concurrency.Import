﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _path;
        private readonly XmlSerializer _serializer;
        public XmlParser(string path)
        {
            _path = path;
            _serializer = new XmlSerializer(typeof(CustomersList));
        }

        public List<Customer> Parse()
        {
            CustomersList list;
            using (Stream reader = new FileStream(_path, FileMode.Open))
            {
                list = (CustomersList)_serializer.Deserialize(reader);          
            }
            return list?.Customers;
        }
    }
}