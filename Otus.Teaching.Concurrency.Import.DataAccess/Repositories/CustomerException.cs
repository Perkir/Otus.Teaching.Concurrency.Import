using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerException :Exception
    {
        public CustomerException(string message)
            : base(message)
        { }
    }
}