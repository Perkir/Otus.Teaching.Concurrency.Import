using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepositoryProxy : ICustomerRepository
    {
        private CustomerRepository _customerRepository;
        private readonly IMemoryCache _cache;
        private DataContext _context;

        public CustomerRepositoryProxy(CustomerRepository customerRepository, DataContext dataContext, IMemoryCache cache)
        {
            _cache = cache;
            _customerRepository = customerRepository;
            _context = dataContext;
        }

        public void Dispose()
        {
            _customerRepository?.Dispose();
        }
        
        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            if (!_cache.TryGetValue(id, out Customer customer))
            {
                customer = await _customerRepository.GetCustomerByIdAsync(id);
                if (customer != null)
                {
                    _cache.Set(customer.Id, customer,
                        new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(5)));
                }
            }
            return customer;
        }

        public async Task<List<Customer>> AddCustomer(IEnumerable<Customer> customers)
        {
            return await _context.Customers.ToListAsync();
        }
        
        public async Task<int> AddCustomerAsync(Customer customer)
        {
            _context.Customers.Add(customer);
            int n = await _context.SaveChangesAsync();
            if (n > 0)
            {
                _cache.Set(customer.Id, customer, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                });
            }
            return 0;
        }
        
        public int CheckUser(Customer customer)
        {
            var customerExist = _context.Customers.FirstOrDefault(c => c.Id == customer.Id);
            if (customerExist != null)
            {
                throw new Exception("UserAlreadyExisted");
            }
            return 0;
        }
        public Task Save()
        {
            throw new System.NotImplementedException();
        }
    }
}