using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;


namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _dataContext;
        public CustomerRepository(DataContext context)
        {
            _dataContext = new DataContext();
        }
        public void AddCustomer(List<Customer> customers)
        {
            foreach (var item in customers)
            {
                _dataContext.Add(item);
            }
            _dataContext.SaveChanges();
  
        }
        public void AddCustomers(List<Customer> customers)
        {
            _dataContext.Customers.AddRange(customers);   
        }
        
        public Task<List<Customer>> AddCustomer(IEnumerable<Customer> customers)
        {
            return (Task<List<Customer>>) _dataContext.Customers.AddRangeAsync(customers); 
        }
        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await _dataContext.Customers.FirstOrDefaultAsync(w => w.Id == id);
        }

        public async Task<int> AddCustomerAsync(Customer customer)
        {
            _dataContext.Add(customer);
            return await _dataContext.SaveChangesAsync();
        }

        public int CheckUser(Customer customer)
        {
            var customerExist = _dataContext.Customers.FirstOrDefault(c => c.Id == customer.Id);
            if (customerExist != null)
            {
               throw new Exception("UserAlreadyExisted");
            }
            return 0;
        }

        public Task Save()
        {
            try
            {
                return _dataContext.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                ex.Entries.Single().Reload();
                return _dataContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return _dataContext.SaveChangesAsync();
            }

        }
        public void Dispose()
        {
            _dataContext?.Dispose();
        }
    }
}