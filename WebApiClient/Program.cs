﻿using System;
using Flurl.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace WebApiClient
{
    class Program
    {
        static async Task Main()
        {
            Console.Write("Введите ID пользователя для добавления: ");
            int.TryParse(Console.ReadLine(), out int customerIdAdd);
            var jsonGenerator = new JsonGenerator(customerIdAdd, "customer.json");
           
            Console.WriteLine(jsonGenerator.CustomerJson);
            try
            {
                var result = await ($"http://localhost:5000/User".WithHeaders(new { Accept = "text/plain", ContentType = "application/json" })
                    .PostJsonAsync(jsonGenerator.CustomerJson));
                Console.WriteLine($"Статус: {result.StatusCode}");
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine($"{ ex.Message }");
            }      
            
            
            Console.WriteLine("Введите Id пользователя для поиска");
            int.TryParse(Console.ReadLine(), out int customerId);

            try
            {
                var result = await $"http://localhost:5000/User/{customerId}".GetJsonAsync();
                ;
                var customer = JsonSerializer.Deserialize<Customer>(result);
                Console.WriteLine($"Пользователь:\n{customer.ToString()} найден");
            }
            catch(FlurlHttpException ex)
            {
                Console.WriteLine($"{ ex.Message }");
            }
            

            Console.ReadLine();

        }
    }
}