using System;
using System.Threading.Tasks;
using NUnit.Framework;
using FakeItEasy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using WebApi.Controllers;

namespace WebApiTests
{
    public class UserControllerTests
    {
        [Test]
        [Description("Запрос пользователя должен завершаться успешно.")]
        public void CanGetUserById()
        { 
            Customer customer = new Customer { Id = 1, Email = "test@test", Phone = "1111", FullName = "fulltest"};
            ICustomerRepository repository = A.Fake<ICustomerRepository>();
            ILogger<UserController> logger = A.Fake<ILogger<UserController>>();
            
            A.CallTo(() => repository.GetCustomerByIdAsync(A<int>._)).Returns(customer);
            
            UserController sut = new UserController(repository, logger);

            var result = sut.Get(1).GetAwaiter().GetResult();
            var okResult = result as ObjectResult;
            
            Assert.NotNull(result);
            Assert.NotNull(okResult);
            Assert.True(okResult is OkObjectResult);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);
        }
        
        [Test]
        [Description("Запрос отсутствующего пользователя должен возвращать не найдено.")]
        public void CanGetNotFoundUserById()
        {
            Customer customer = null;
            ICustomerRepository repository = A.Fake<ICustomerRepository>();
            ILogger<UserController> logger = A.Fake<ILogger<UserController>>();
            
            A.CallTo(() => repository.GetCustomerByIdAsync(A<int>._)).Returns(customer);
            
            UserController sut = new UserController(repository, logger);

            var result = sut.Get(1).GetAwaiter().GetResult();
            var notFoundResult = result as ObjectResult;
            
            Assert.NotNull(result);
            Assert.NotNull(notFoundResult);
            Assert.True(notFoundResult is NotFoundObjectResult);
            Assert.AreEqual(StatusCodes.Status404NotFound, notFoundResult.StatusCode);
        }
        
        [Test]
        [Description("Запрос создания пользователя должен завершаться успешно.")]
        public void CanPostCustomer()
        {
            Customer customer = new Customer { Id = 1, Email = "test@test", Phone = "1111", FullName = "fulltest"};
            ICustomerRepository repository = A.Fake<ICustomerRepository>();
            ILogger<UserController> logger = A.Fake<ILogger<UserController>>();
            
            A.CallTo(() => repository.CheckUser(A<Customer>._)).Returns(0);

            UserController sut = new UserController(repository, logger);

            var result = sut.Post(customer).GetAwaiter().GetResult();
            var okResult = result as ObjectResult;
            
            Assert.NotNull(result);
            Assert.NotNull(okResult);
            Assert.True(okResult is OkObjectResult);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);
        }
        
        [Test]
        [Description("Запрос создания пользователя если пользователь уже есть в бд должен завершаться с ошибкой.")]
        public void CantPostCustomer()
        {
            Customer customer = new Customer { Id = 1, Email = "test@test", Phone = "1111", FullName = "fulltest"};
            ICustomerRepository repository = A.Fake<ICustomerRepository>();
            ILogger<UserController> logger = A.Fake<ILogger<UserController>>();

            A.CallTo(() => repository.CheckUser(A<Customer>._))
                .Throws(new CustomerException("UserAlreadyExisted"));

            UserController sut = new UserController(repository, logger);

            var result = sut.Post(customer).GetAwaiter().GetResult();
            var ConflictResult = result as ObjectResult;
            
            Assert.NotNull(result);
            Assert.NotNull(ConflictResult);
            Assert.True(ConflictResult is ConflictObjectResult);
            Assert.AreEqual(StatusCodes.Status409Conflict, ConflictResult.StatusCode);
        }
    }
}